import React, {useState, useContext } from 'react';
import { Loader } from "./Loader";
import { notUsedCaseesInfo } from "../Utils/apiResolvers";
import { store } from "../App";
import { isLockedButtons } from '../Redux/actions'
import { AlertContext } from "../Context/Alert/alertContext";


export const UnusedCases = () => {

    const [cases, setCases] = useState([]);
    const [loader, setLoader] = useState(false);
    const alert = useContext(AlertContext);


    const runLogic = async (event) => {
        setCases([]);
        event.preventDefault();
        const { projectStore } = store.getState();
        if (!projectStore.selectedProject){
            alert.show('Select project, please', 'danger');
            return;
        }
        store.dispatch(isLockedButtons(true));
        setLoader(true);
        const resp = await notUsedCaseesInfo(projectStore.selectedProject);
        if (resp.length === 0) alert.show("Not Found Unused Cases");
        setLoader(false);
        store.dispatch(isLockedButtons(false));
        setCases(resp)
    };

    const { lockButtons } = store.getState();



    return (
        <div>
            <div>
                <button type="submit" className="btn btn-primary buttons" onClick={runLogic} disabled={lockButtons}>Find Unused Cases</button>
                {loader ? <Loader/> : null}
            </div>
                <div className="List">
                    {cases.map(el =>  <div key={el.id}>{el.id} - {el.title}</div>)}
                </div>
        </div>
    )
};


