import React, { useState } from 'react'
import Select from 'react-select'
import { store } from "../App";
import { setSelectedProject } from "../Redux/actions";
import { UnusedCases } from './UnusedCases'

export const AllProjects = ({projects}) => {

    const options = projects.map( (pr) => ({
        value: pr.id,
        label: pr.name,
    }));


    const [selected, setSelected] = useState('');

    const handleSelect = (option) => {
        setSelected(option);
        store.dispatch(setSelectedProject(option.value));
    };


    return (
        <div>
            <small id="hint" className="form-text text-muted">Select project from the list</small>
            <Select options={options}
                    value={selected}
                    onChange={handleSelect}
            />
        </div>
    )
};
