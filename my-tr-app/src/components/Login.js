import {AllProjects} from "./AllProjects";
import React, {useContext, useState} from "react";
import { getAllProjects } from "../Utils/apiResolvers";
import useInput from '../CustomHooks/UseInput'
import { AlertContext } from "../Context/Alert/alertContext";
import { store } from "../App";
import { setAuth, setAllProjects } from "../Redux/actions";

export const Login = () => {

    const alert = useContext(AlertContext);

    const email = useInput('');
    const password = useInput('');
    const [projects, setProjects] = useState([]);
    const [isVerified, setIsVerified] = useState(false);
    const [lockBtn, setLocked] = useState(false);

    store.subscribe(() => {
        const { lockButtons } = store.getState();
        setLocked(lockButtons);
    });

    const verifyProjcets = async (event) => {
        event.preventDefault();

        store.dispatch(setAuth({
            email: email.value,
            password: password.value,
        }));

        const availableProjects = await getAllProjects();
        if (Array.isArray(availableProjects) && availableProjects.length) {
            alert.show('Login Success.', 'success');
            store.dispatch(setAllProjects(availableProjects));
            setProjects(availableProjects);
            setIsVerified(true)
        }
        else {
            alert.show(availableProjects, 'danger')
        }
    };






    return (
        <form>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Login</label>
                <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Email'
                       value={email.value}
                       onChange={email.onChange}
                />
                <small id="emailHelp" className="form-text text-muted">Enter your login (email) here </small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input type="password" className="form-control" id="exampleInputPassword1"
                       value={password.value}
                       onChange={password.onChange}
                />
            </div>
            <button type="submit" className="btn btn-primary" onClick={verifyProjcets} disabled={lockBtn}>Verify Available Projects</button>

            {isVerified ? <AllProjects projects={projects}/> : null}
        </form>
    )
};
