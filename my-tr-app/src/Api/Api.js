import axios from 'axios';
import { store } from '../App';
// axios.defaults.baseURL = 'https://apiko.testrail.io/index.php?/api/v2';

axios.defaults.headers = {
    //'Authorization': `Basic ${localStorage.getItem('_token')}`,
    'Content-Type': 'application/json',
};

const basicAuth = () => {
    const {authStore} = store.getState();
    return {
        username: authStore.email,
        password: authStore.password,
    }
} ;

export const getProjects = async () => {
    return axios(`get_projects`, {
        data: {
            id : 1,
            title: "An example test case"
        },
        auth: basicAuth(),
    });
};

export const getCases = async (project_id) => {
    return axios(`get_cases/${project_id}`, {
        data: {
            id : 1,
            title: "An example test case"
        },
        auth: basicAuth(),
    });
};

export const getRuns = async (project_id) => {
    return axios(`/get_runs/${project_id}`, {
        data: {
            id : 1,
            title: "An example test case"
        },
        auth: basicAuth(),
    });
};

export const getTestsByRunId = async (run_id) => {
    return axios(`get_tests/${run_id}`, {
        data: {
            id : 1,
            title: "An example test case"
        },
        auth: basicAuth(),
    });
};

export const getCase = async (case_id) => {
    return axios(`get_case/${case_id}`, {
        data: {
            id : 1,
            title: "An example test case"
        },
        auth: basicAuth(),
    });
};
