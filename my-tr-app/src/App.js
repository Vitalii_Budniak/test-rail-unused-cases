import React from 'react';
import { UnusedCases } from './components/UnusedCases'
import { Login } from './components/Login'
import { AlertState } from "./Context/Alert/AlertState";
import { Alert } from "./components/Alert";
import { createStore } from "redux";
import { rootReducer } from "./Redux/rootReducer";
import './App.css';


//Redux store
export const store = createStore(rootReducer);

function App() {

  return (
    <div className="container">
        <AlertState>
            <Alert/>
            <Login/>
            <UnusedCases/>
        </AlertState>
    </div>
  );
}

export default App;
