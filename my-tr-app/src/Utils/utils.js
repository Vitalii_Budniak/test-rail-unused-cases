const onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
};

export const flatten = arr => arr.reduce((acc, subArr) => acc.concat(subArr), []);

export const unique = (arr) => arr.filter( onlyUnique );

export const intersection = (arrayOne, arrayTwo) => {
    let big = arrayOne;
    let small = arrayTwo;
    if (arrayOne.length < arrayTwo.length) {
        big = arrayTwo;
        small = arrayOne;
    }
    return big.filter(element => !small.includes(element));
};
