import { getProjects, getCases, getRuns, getTestsByRunId } from '../Api/Api'
import { flatten, unique, intersection } from './utils'
import { store } from '../App';
import { saveAllCases } from "../Redux/actions";

export const getAllProjects = async () => {
    try {
        const { data } = await getProjects();
        if (data){
            return data.map(project => {
                return {
                    id: project.id,
                    name: project.name,
                }
            })
        }
    }catch (e) {
        return e.message;
    }
};


const allTestCases = async (projectId) => {
    const {data} = await getCases(projectId);
    const saveToStore = data.map(el => {
        return {
            id: el.id,
            title: el.title
        }
    });
    //localStorage.setItem("tests", JSON.stringify(saveToStore));
    store.dispatch(saveAllCases(saveToStore));
    return data.map(testCase => testCase.id)
};

const allTestRuns = async (projectId) => {
    const {data} = await getRuns(projectId);
    return data.map(run => run.id)
};

const getUniqTestsForAllRuns = async (projectId) => {
    const runIds = await allTestRuns(projectId);

    const resp = runIds.map(async(run) =>{
        const {data} =  await getTestsByRunId(run);
        return data;
    });

    const resolved  = await Promise.all(resp);

    const getOnlyIds = resolved.map( (subarray) => {
        return subarray.map(el => el.case_id)
    });

    const concatArrays = flatten(getOnlyIds);

    return unique(concatArrays)
};

export const findDifference = async (projectId) => {
    const response1 =  await getUniqTestsForAllRuns(projectId);
    const response2 = await allTestCases(projectId);
    return intersection(response1, response2);
};

export const notUsedCaseesInfo = async (projectId) => {
    // const saved = JSON.parse(await localStorage.getItem('tests'));
    const ids = await findDifference(projectId);
    const { casesStore } = store.getState();
    const result =  ids.map(id => {
        return casesStore.filter((savedEl) => {
            if (savedEl.id === id){
                return savedEl;
            }
        })
    });
    return  flatten(result)
};
