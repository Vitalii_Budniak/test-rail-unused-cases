export const SET_AUTH = "SET_AUTH";
export const ALL_CASES = "ALL_CASES";
export const SET_PROJECTS = "SET_PROJECTS";
export const SET_SELECTED_PROJECT_ID = "SET_SELECTED_PROJECT_ID";
export const LOCKED_BTNS = "LOCKED_BTNS";

