import { SET_AUTH, ALL_CASES, SET_PROJECTS, SET_SELECTED_PROJECT_ID, LOCKED_BTNS } from './types';


export const setAuth = (data) => ({ type: SET_AUTH, credentials:data });

export const saveAllCases = (cases) => ({ type: ALL_CASES, cases });

export const setAllProjects = (projects) => ({ type: SET_PROJECTS, projects });

export const setSelectedProject = (projectId) => ({ type: SET_SELECTED_PROJECT_ID, projectId });

export const isLockedButtons = (isLocked) => ({ type: LOCKED_BTNS, isLocked });

