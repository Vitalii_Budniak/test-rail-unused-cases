import { SET_AUTH, ALL_CASES, SET_PROJECTS, SET_SELECTED_PROJECT_ID, LOCKED_BTNS } from './types';
import { combineReducers } from "redux";


const credentials = {
    email: '',
    password: '',
};

const auth = (state=credentials, action) => {
    if (action.type === SET_AUTH) {
        return state={
            email: action.credentials.email,
            password: action.credentials.password,
        };
    }
    return state
};

const allCases = (state=[], action) => {
    if (action.type === ALL_CASES){
        return state = action.cases
    }
    return state;
};


let initialProjectsStore = {
    projects: [],
    selectedProject: '',
};

const projectStore = (state=initialProjectsStore, action) => {
    if (action.type === SET_SELECTED_PROJECT_ID){
        state.selectedProject = action.projectId
    }else if (action.type === SET_PROJECTS) {
        state.projects = action.projects
    }
    return state;
};



const lockedBtns = (state=false, action) => {
    if (action.type === LOCKED_BTNS){
        state = action.isLocked;
    }
    return state
};



export const rootReducer = combineReducers({
    authStore: auth,
    casesStore: allCases,
    projectStore: projectStore,
    lockButtons: lockedBtns,
});
