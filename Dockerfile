FROM node:carbon-slim


# copy our application code
ADD my-tr-app /work/react-app
WORKDIR /work/react-app

RUN npm rebuild node-sass
RUN yarn install

EXPOSE 3000


CMD ["yarn", "start"]
